#!/usr/bin/env python3
#
# This file makes use of telnetlib.
#
# You should have received a copy of the GNU Lesser General Public License
# along with Paramiko; if not, write to the Free Software Foundation, Inc.,
# 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA.

import getpass
import os
import traceback
import argparse
import time
import subprocess
import math
import datetime
from multiprocessing.dummy import Pool as ThreadPool
import collections
import csv
import telnetlib
import pexpect

import yaml
import paramiko


def ask_pass(user):
    """
    Get user to type in password
    :param str user: Username
    :return: Password (`str`)
    """
    prompt = "Password for {} to SSH to OLT devices: ".format(user)
    password = getpass.getpass(prompt)
    return password


def write_result_to_file(device_type, log_file_name, log_content):
    """
    Append ``log_content_str`` to ``log_file_name`` with timestamp
    :param str device_type: device type to use as log folder name
    :param str log_file_name: name of log file in current working directory
    :param list log_content: log content to write to log file
    :return: ``None``
    """
    pwd = os.getcwd()
    if not os.path.isdir(device_type + '-results'):
        os.makedirs(device_type + '-results')
    item_full_path = os.path.join(pwd, device_type + '-results/' + log_file_name + '-result.yml')

    with open(item_full_path, 'w') as outfile:
        yaml.dump(log_content, outfile, default_flow_style=False)
    return


class TelnetOltViaAten(object):
    def __init__ (self,aten, port, username, password):
        self.aten = aten
        self.telnet_port = 5000 + port
        self.username = username
        self.password = password
        self.port = port
        """
        Result of the serial port check in a dictionary with the following keys/values
        serial_status
            'working': the serial port connectivity is in place
            'working-with-not-check': the serial port connectivity couldn't be checked
        device_subtype
            'cisco': the device connecting to the serial port is a Cisco device
            'unknown': the device type of the device connecting to the serial port is unknown
        serial_substatus
            'no-cs-login-prompt': No login prompt is seen when telnet to ATEN console server
            'no-cs-password-prompt': No password prompt is seen when telnet to ATEN console server
            'serial-port-busy': Serial port is being used by a different session 
            'cs-rad-auth-failed': Authentication to ATEN console server using RADIUS account fails
            'working-with-rad-auth': Authentication to the device connecting to the serial port works with RADIUS 
            'working-with-local-account': Authentication to the device connecting to the serial port works with local account
        """
        self.summary_result = {}
        self.log_file_name = aten
        try:
            self.check()
        except:
            self.summary_result = {'serial_status': 'could-not-check', 'device_subtype': 'unknown',
                                   'serial_substatus': 'unknown'}
            pass

    def log(self, log_content_str):
        """
        Append ``log_content`` to ``log_file_name`` with timestamp
        :param str log_content_str: log content to write to log file
        :return: ``None``
        """
        pwd = os.getcwd()
        if not os.path.isdir('cons' + '-logs'):
            os.makedirs('cons' + '-logs')
        item_full_path = os.path.join(pwd, 'cons' + '-logs/' + self.log_file_name + '.log')

        ts = time.time()
        st = datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d %H:%M:%S')

        INDENT = " "

        log_content_list = log_content_str.split('\r\n')

        with open(item_full_path, 'a') as x:
            x.write('\n[' + st + ']\n')
            for line in log_content_list:
                x.write(INDENT * 25 + line + '\n')

    def check(self):
        """
        Main function to check serial port status. Main logic is as below
        1. Telnet to ATEN console server port port 5000+ using RADIUS account
            Stop if
                Telnet connection is refused
                Telnet connections times out
                Expected login/password prompt is not seen
                Serial port is busy
        2. Log in to end device of the serial port using RADIUS account
        3. Log in to end device of the serial port using local account
        :return:
        """
        # Telnet to ATEN serial port
        try:
            tn = telnetlib.Telnet(self.aten, self.telnet_port, timeout=8)
        except:
            self.summary_result = {'serial_status': 'could-not-check', 'device_subtype': 'unknown',
                                   'serial_substatus': 'unknown'}
            return

        # Log in with RADIUS account, should see 'Suspend Menu.' if successful
        tn_read = tn.read_until(b"Login:", 4)
        print(tn_read)
        self.log(tn_read.decode('utf-8'))

        # Login prompt is not seen
        if "Login:" not in tn_read.decode('utf-8'):
            self.summary_result = {'serial_status': 'could-not-check', 'device_subtype': 'unknown',
                                   'serial_substatus': 'no-cs-login-prompt'}
            return

        tn.write(self.username.encode('ascii') + b"\r")
        tn_read = tn.read_until(b"Password:", 4)
        print(tn_read)
        self.log(tn_read.decode('utf-8'))

        # Password prompt is not seen
        if "Password:" not in tn_read.decode('utf-8'):
            self.summary_result = {'serial_status': 'could-not-check', 'device_subtype': 'unknown',
                                   'serial_substatus': 'no-cs-password-prompt'}
            return

        tn.write(self.password.encode('ascii') + b"\r")
        tn_read = tn.read_until('Suspend Menu.'.encode('ascii'), 9)
        print(tn_read)
        self.log(tn_read.decode('utf-8'))

        # If see 'busy', serial port is busy
        if "busy" in tn_read.decode('utf-8'):
            self.log("Serial port is busy")
            self.summary_result = {'serial_status': 'could-not-check', 'device_subtype': 'unknown',
                                   'serial_substatus': 'serial-port-busy'}

            # Press '\r'
            tn.write(b"\r")
            tn_read = tn.read_until(b'one: ',4)
            print(tn_read)
            self.log(tn_read.decode('utf-8'))

            # Press 'q'
            tn.write("q".encode('ascii'))
            print(tn_read)
            self.log(tn_read.decode('utf-8'))

            tn.close()
            return

        # If don't see 'Suspend Menu.', RADIUS authentication doesn't work, need manual fallout
        if "Suspend Menu." not in tn_read.decode('utf-8'):
            self.log("RADIUS authentication doesn't work, need manual fallout")
            self.summary_result = {'serial_status': 'could-not-check', 'device_subtype': 'unknown',
                                   'serial_substatus': 'cs-rad-auth-failed'}

            # Press 'CTRL-C'
            tn.write(chr(3).encode('ascii'))
            tn.close()
            return

        # Hit enter, should see 'login:'
        tn.write(b"\r")
        y = tn.read_until(b'login: ', 25)
        print(y)
        self.log(y.decode('utf-8'))
        """
        if see 'cov-olt-1 login: ', it's E9, could be active card or standby card
        if see >, it's E7, type 'exit', will see 'Connection closed by foreign host\r', type '\r' will see 'Entering character mode\rEscape character is '^]'.\rcov744-olt-1" Fri Nov  8 11:08:22 2019
        if see "Username: ", it's E7. If trying to log in, should type '\r' multiple times until see 'Connection closed by foreign host' and 'Username: ' then it's safe to put in username/password
        if see #, it's inside a working E9 session, type 'CTRL-Z' then 'exit', should see 'cov744-olt-2 login: '
        if see "Password: "
           if this is active card on E9, type '\r', wait 5 secs, should see 'Login incorrect \r cov744-olt-2 login:'
           if this is standby card on E9, type '\r', wait 15 secs, should see another "Password: ", type '\r', wait 5 secs, should see 'Login incorrect \r cov744-olt-2 login:'
           if this is on E7, type '\r' up to 4 times, should see 'Connection closed by foreign host', then type '\r', should see 'Entering character mode\rEscape character is '^]'.\r\r"cov744-olt-1" Fri Nov  8 11:20:18 2019\r\r
        
        if see "Password: "
          Press CTRL-C
              if see "Password: ", it's E7-20
              if see 'cov744-olt-2 login: ' it's E9, active or standby card
           if this is active card on E9, type '\r', wait 5 secs, should see 'Login incorrect \r cov744-olt-2 login:'
           if this is standby card on E9, type '\r', wait 15 secs, should see another "Password: ", type '\r', wait 5 secs, should see 'Login incorrect \r cov744-olt-2 login:'
           if this is on E7, type '\r' up to 4 times, should see 'Connection closed by foreign host', then type '\r', should see 'Entering character mode\rEscape character is '^]'.\r\r"cov744-olt-1" Fri Nov  8 11:20:18 2019\r\r
        """
        # A Calix E9-2 being commissioned
        if "CLX3001 login: " in y.decode('utf-8'):
            print("Initial prompt of the device is {}".format(y.decode('utf-8').splitlines()))
            print("Calix OLT E9-2 being commissioned, ignore")
            self.log("Initial prompt of the device is {}".format(y.decode('utf-8').splitlines()))
            self.log("Calix OLT E9-2 being commissioned, ignore")

            self.summary_result = {'serial_status':'working','device_subtype':'calix-e9','serial_substatus':'being-commissioned'}

            # Press 'CTRL-D'
            tn.write(chr(4).encode('ascii'))
            tn_read = tn.read_until(b'one: ', 4)
            print(tn_read)
            self.log(tn_read.decode('utf-8'))

            # Press 'q'
            tn.write("q".encode('ascii'))

            tn.close()
            return
        # Happy path, try RADIUS account
        elif "login: " in y.decode('utf-8'):
            print("Initial prompt of the device is {}".format(y.decode('utf-8').splitlines()))
            print("Normal E9-2 session, can be active card or standby card")
            self.log("Initial prompt of the device is {}".format(y.decode('utf-8').splitlines()))
            self.log("Normal E9-2 session, can be active card or standby card")
            tn.write(self.username.encode('ascii') + b"\r")
            tn_read = tn.read_until(b"Password: ", 4)
            print(tn_read)
            self.log(tn_read.decode('utf-8'))

            tn.write(self.password.encode('ascii') + b"\r")
            tn_read = tn.read_until('#'.encode('ascii'), 30)
            print(tn_read)
            self.log(tn_read.decode('utf-8'))
            # Happy path, RADIUS works
            if "#" in tn_read.decode('utf-8'):
                print("Calix OLT E9-2, active card")
                self.log("Calix OLT E9-2, active card")
                self.summary_result = {'serial_status': 'working', 'device_subtype': 'calix-e9',
                                       'serial_substatus': 'active-card-rad-auth'}
                # Press 'exit'
                tn.write("exit".encode('ascii') + b"\r")
                tn_read = tn.read_until(b'login: ', 4)
            # RADIUS account doesn't work, try local account
            elif "incorrect" in tn_read.decode('utf-8'):
                print("RADIUS account doesn't work, trying local account")
                self.log("RADIUS account doesn't work, trying local account")

                tn.write('cfadmin1'.encode('ascii') + b"\r")
                tn_read = tn.read_until(b"Password: ", 4)
                print(tn_read)
                self.log(tn_read.decode('utf-8'))

                tn.write("2-15-B3df0rd!".encode('ascii') + b"\r")
                tn_read = tn.read_until('#'.encode('ascii'), 30)
                print(tn_read)
                self.log(tn_read.decode('utf-8'))
                # Local account works
                if "#" in tn_read.decode('utf-8'):
                    print("Calix OLT E9-2, standby card probably")
                    self.log("Calix OLT E9-2, standby card probably")
                    self.summary_result = {'serial_status': 'working', 'device_subtype': 'calix-e9',
                                           'serial_substatus': 'standby-card-probably-local-account'}
                    # Press 'exit'
                    tn.write("exit".encode('ascii') + b"\r")
                    tn_read = tn.read_until(b'login: ', 4)
                else:
                    print("Manual fallout")
                    self.log("Manual fallout")
                    self.summary_result = {'serial_status': 'working', 'device_subtype': 'calix-e9',
                                           'serial_substatus': 'unknown'}
            else:
                print("Not sure what this is, need to check further")
                self.log("Not sure what this is, need to check further")
                self.summary_result = {'serial_status': 'working', 'device_subtype': 'unknown',
                                       'serial_substatus': 'unknown'}

            # Press 'CTRL-D'
            tn.write(chr(4).encode('ascii'))
            tn_read = tn.read_until(b'one: ', 4)
            print(tn_read)
            self.log(tn_read.decode('utf-8'))

            # Press 'q'
            tn.write("q".encode('ascii'))

            tn.close()
            return
        # Calix E7-20
        elif "Username: " in y.decode('utf-8'):
            print("Initial prompt of the device is {}".format(y.decode('utf-8').splitlines()))
            print("Calix OLT E7-20 - ignore")
            self.log("Initial prompt of the device is {}".format(y.decode('utf-8').splitlines()))
            self.log("Calix OLT E7-20 - ignore")

            self.summary_result = {'serial_status': 'working', 'device_subtype': 'calix-e7',
                                   'serial_substatus': 'auth-not-checked'}

            # Press 'CTRL-D'
            tn.write(chr(4).encode('ascii'))
            tn_read = tn.read_until(b'one: ', 4)
            print(tn_read)
            self.log(tn_read.decode('utf-8'))

            # Press 'q'
            tn.write("q".encode('ascii'))

            tn.close()
            return
        # A working session on a Calix OLT E9-2 which somebody forgot to fully exit
        elif "#" in y.decode('utf-8'):
            print("Initial prompt of the device is {}".format(y.decode('utf-8')))
            self.log("Initial prompt of the device is {}".format(y.decode('utf-8')))
            # Fully exit the session by hitting 'CTRL-Z' then 'exit'
            # Press 'CTRL-Z'
            tn.write(chr(26).encode('ascii'))
            tn_read = tn.read_until(b'#', 4)

            # Press 'exit'
            tn.write("exit".encode('ascii') + b"\r")
            tn_read = tn.read_until(b'login: ', 4)

            # Use RADIUS account
            tn.write(self.username.encode('ascii') + b"\r")
            tn_read = tn.read_until(b"Password: ", 4)
            print(tn_read)
            self.log(tn_read.decode('utf-8'))

            tn.write(self.password.encode('ascii') + b"\r")
            tn_read = tn.read_until('#'.encode('ascii'), 30)
            print(tn_read)
            self.log(tn_read.decode('utf-8'))
            # RADIUS account works
            if "#" in tn_read.decode('utf-8'):
                print("Calix OLT E9-2, active card")
                self.log("Calix OLT E9-2, active card")
                self.summary_result = {'serial_status': 'working', 'device_subtype': 'calix-e9',
                                       'serial_substatus': 'active-card'}
                # Press 'exit'
                tn.write("exit".encode('ascii') + b"\r")
                tn_read = tn.read_until(b'login: ', 4)
            # RADIUS account doesn't work
            elif "incorrect" in tn_read.decode('utf-8'):
                print("RADIUS account doesn't work, trying local account")
                self.log("RADIUS account doesn't work, trying local account")

                tn.write('cfadmin1'.encode('ascii') + b"\r")
                tn_read = tn.read_until(b"Password: ", 4)
                print(tn_read)
                self.log(tn_read.decode('utf-8'))

                tn.write("2-15-B3df0rd!".encode('ascii') + b"\r")
                tn_read = tn.read_until('#'.encode('ascii'), 30)
                print(tn_read)
                self.log(tn_read.decode('utf-8'))
                # Local account works
                if "#" in tn_read.decode('utf-8'):
                    print("Calix OLT E9-2, standby card probably")
                    self.log("Calix OLT E9-2, standby card probably")
                    self.summary_result = {'serial_status': 'working', 'device_subtype': 'calix-e9',
                                           'serial_substatus': 'standby-card-probably-local-account'}
                    # Press 'exit'
                    tn.write("exit".encode('ascii') + b"\r")
                    tn_read = tn.read_until(b'login: ', 4)
                else:
                    print("Manual fallout")
                    self.log("Manual fallout")
                    self.summary_result = {'serial_status': 'working', 'device_subtype': 'calix-e9',
                                           'serial_substatus': 'unknown'}
            else:
                print("Not sure what this is, need to check further")
                self.log("Not sure what this is, need to check further")
                self.summary_result = {'serial_status': 'working', 'device_subtype': 'unknown',
                                       'serial_substatus': 'unknown'}

            # Press 'CTRL-D'
            tn.write(chr(4).encode('ascii'))
            tn_read = tn.read_until(b'one: ', 4)
            print(tn_read)
            self.log(tn_read.decode('utf-8'))

            # Press 'q'
            tn.write("q".encode('ascii'))

            tn.close()
        # A working session to Calix E7-20 which someone forgot to fully exit
        elif ">" in y.decode('utf-8'):
            print("Initial prompt of the device is {}".format(y.decode('utf-8')))
            self.log("Initial prompt of the device is {}".format(y.decode('utf-8')))

            self.summary_result = {'serial_status': 'working', 'device_subtype': 'calix-e7',
                                   'serial_substatus': 'unknown'}

            # Press 'exit', expect to see 'Connection closed by foreign host'
            tn.write("exit".encode('ascii') + b"\r")
            tn_read = tn.read_until(b'Connection closed by foreign host', 4)

            # Press enter, expect to see 'Username: '
            tn.write(b"\r")
            tn_read = tn.read_until(b'Username: ', 4)

            # Press 'CTRL-D'
            tn.write(chr(4).encode('ascii'))
            tn_read = tn.read_until(b'one: ', 4)
            print(tn_read)
            self.log(tn_read.decode('utf-8'))

            # Press 'q'
            tn.write("q".encode('ascii'))

            tn.close()
        # A session someone left after typing username at prompt
        elif "Password: " in y.decode('utf-8'):
            print("Initial prompt of the device is {}".format(y.decode('utf-8').splitlines()))
            self.log("Initial prompt of the device is {}".format(y.decode('utf-8').splitlines()))

            # Press CTRL-C
            tn.write(chr(3).encode('ascii'))
            tn_read = tn.read_until(b"login: ", 1)

            if "login: " in tn_read.decode('utf-8'):
                print("Calix E9-2, active or standby")
                self.log("Calix E9-2, active or standby")

                tn.write(self.username.encode('ascii') + b"\r")
                tn_read = tn.read_until(b"Password: ", 4)
                # print(x)

                tn.write(self.password.encode('ascii') + b"\r")
                tn_read = tn.read_until('#'.encode('ascii'), 30)
                # print(x)

                if "#" in tn_read.decode('utf-8'):
                    print("Calix OLT E9-2, active card")
                    self.log("Calix OLT E9-2, active card")
                    self.summary_result = {'serial_status': 'working', 'device_subtype': 'calix-e9',
                                           'serial_substatus': 'active-card'}
                    # Press 'exit'
                    tn.write("exit".encode('ascii') + b"\r")
                    tn_read = tn.read_until(b'login: ', 4)
                elif "incorrect" in tn_read.decode('utf-8'):
                    print("RADIUS account doesn't work, trying local account")
                    self.log("RADIUS account doesn't work, trying local account")

                    tn.write('cfadmin1'.encode('ascii') + b"\r")
                    tn_read = tn.read_until(b"Password: ", 4)
                    print(tn_read)
                    self.log(tn_read.decode('utf-8'))

                    tn.write("2-15-B3df0rd!".encode('ascii') + b"\r")
                    tn_read = tn.read_until('#'.encode('ascii'), 30)
                    print(tn_read)
                    self.log(tn_read.decode('utf-8'))
                    # Local account works
                    if "#" in tn_read.decode('utf-8'):
                        print("Calix OLT E9-2, standby card probably")
                        self.log("Calix OLT E9-2, standby card probably")
                        self.summary_result = {'serial_status': 'working', 'device_subtype': 'calix-e9',
                                               'serial_substatus': 'standby-card-probably-local-account'}
                        # Press 'exit'
                        tn.write("exit".encode('ascii') + b"\r")
                        tn_read = tn.read_until(b'login: ', 4)
                    else:
                        print("Manual fallout")
                        self.log("Manual fallout")
                        self.summary_result = {'serial_status': 'working', 'device_subtype': 'calix-e9',
                                               'serial_substatus': 'unknown'}
                else:
                    print("Not sure what this is, need to check further")
                    self.log("Not sure what this is, need to check further")
                    self.summary_result = {'serial_status': 'working', 'device_subtype': 'unknown',
                                           'serial_substatus': 'unknown'}

                # Press 'CTRL-D'
                tn.write(chr(4).encode('ascii'))
                tn_read = tn.read_until(b'one: ', 4)
                print(tn_read)
                self.log(tn_read.decode('utf-8'))

                # Press 'q'
                tn.write("q".encode('ascii'))

                tn.close()
                return
            else:
                print("Not sure what this is, need to check further")
                self.log("Not sure what this is, need to check further")
                self.summary_result = {'serial_status': 'working', 'device_subtype': 'unknown',
                                       'serial_substatus': 'unknown'}

                # Press 'CTRL-D'
                tn.write(chr(4).encode('ascii'))
                tn_read = tn.read_until(b'one: ', 4)
                print(tn_read)
                self.log(tn_read.decode('utf-8'))

                # Press 'q'
                tn.write("q".encode('ascii'))

                tn.close()
                return

        else:
            print("Initial prompt of the device is {}".format(y.decode('utf-8').splitlines()))
            print("Manual fallout or fail console port")
            self.log("Initial prompt of the device is {}".format(y.decode('utf-8').splitlines()))
            self.log("Manual fallout or fail console port")
            self.summary_result = {'serial_status': 'unknown', 'device_subtype': 'unknown',
                                   'serial_substatus': 'unknown'}
            # Press 'CTRL-D'
            tn.write(chr(4).encode('ascii'))
            tn_read = tn.read_until(b'one: ', 4)
            print(tn_read)
            self.log(tn_read.decode('utf-8'))

            # Press 'q'
            tn.write("q".encode('ascii'))

            tn.close()
            return


class TelnetAten(object):
    def __init__(self, aten, username, password):
        self.serial_port_list = []
        self.aten = aten
        self.username = username
        self.password = password
        try:
            self.retrieve_serial_port_list()
        except:
            pass

    def retrieve_serial_port_list(self):
        try:
            tn = telnetlib.Telnet(self.aten, port=23, timeout=8)
        except Exception as e:
            # print("Exception encountered 2")
            return

        # Enter username and password to log in to ATEN
        tn.read_until(b"Login:", 4)

        tn.write(self.username.encode('ascii') + b"\r")
        tn.read_until(b"Password:", 4)
        tn.write(self.password.encode('ascii') + b"\r")
        read_output = tn.read_until(b'one:', 4)
        print(read_output.decode('utf-8').split('\r\n'))

        # Check that 'Port Access' is number 3 in ATEN menu
        for line in read_output.decode('utf-8').split('\r\n'):
            if "3.  Port Access" in line.strip():
                # time.sleep(2)
                # print(aten + " - yes")
                break

        # Go to 'Port Access', read serial port list and exit
        tn.write("3".encode('ascii') + b"\r")
        read_output = tn.read_until(b'Q. Exit', 4)
        tn.write("q".encode('ascii'))
        tn.write("q".encode('ascii'))

        cs_serial_port_list = []

        # Remove unused serial ports from serial port name list
        for line in read_output.decode('utf-8').split('\r'):
            cs_serial_ports = line.strip().split(". ")
            if len(cs_serial_ports) > 1:
                if "COM" not in cs_serial_ports[1] and "Exit" not in cs_serial_ports[1]:
                    # cs_serial_port_list.append({cs_serial_ports[1]: cs_serial_ports[0]})
                    cs_serial_port_list.append({'name':cs_serial_ports[1],'port':cs_serial_ports[0]})

        self.serial_port_list = cs_serial_port_list

        # return cs_serial_port_list

    def check_a_serial_port_in_aten(self, device_full_dns_name):
        # Retrieve 'abd860-olt-2' from 'abd860-olt-2.network.cityfibre.com'
        device_simple_name = str(device_full_dns_name.split('.')[0])

        # Retrieve 'olt-2' from 'abd860-olt-2'
        device_name_without_site_name = device_simple_name.split('-',1)[1]

        # Retrieve 'abd860' from 'abd860-olt-2'
        site_name = device_simple_name.split('-',1)[0]

        for item in self.serial_port_list:
            # Look in serial ports containing 'olt-2'
            if device_name_without_site_name in item['name'] and "0" not in item['name']:
                x = TelnetOltViaAten(self.aten,int(item['port']),self.username, self.password)
                print(x.summary_result)
                x.summary_result['label'] = device_name_without_site_name
                write_result_to_file('olt-selective-cons', site_name + "-port-" + item['port'], x.summary_result)


def check_all_serial_ports_in_aten(console_variable_list):
    cs_name = console_variable_list[0]
    username = console_variable_list[1]
    password = console_variable_list[2]
    try:
        # Telnet to ATEN and retrieve device serial port list
        cs_serial_port_list = telnet_to_aten(cs_name, username, password)

        # Retrieve site name from ATEN DNS name
        site_name = str(cs_name.split('.')[0].split('-')[0])

        # Initiate a dictionary to store site serial port details
        site_serial_port_details = {'site': site_name, 'devices': []}

        # Classify serial ports into device types
        for item in cs_serial_port_list:
            for k, v in item.items():
                if "mgmt" in k.lower() and "mgmtsw" not in k.lower() and "olt" not in k.lower():
                    site_serial_port_details['devices'].append({'name': k.lower(), 'console_port': v, 'device_type': 'cisco_887'})
                if "mgmtsw" in k.lower():
                    site_serial_port_details['devices'].append({'name': k.lower(), 'console_port': v, 'device_type': 'cisco_2960'})
                if "rtr" in k.lower():
                    site_serial_port_details['devices'].append({'name': k.lower(), 'console_port': v, 'device_type': 'nokia_a8'})
                if "fw" in k.lower():
                    site_serial_port_details['devices'].append({'name': k.lower(), 'console_port': v, 'device_type': 'fw'})
                if "olt" in k.lower() and "spare" not in k.lower():
                    site_serial_port_details['devices'].append({'name': k.lower(), 'console_port': v, 'device_type': 'olt'})

        # Initiate a list to store serial port status check results
        cons_result = []

        # Check serial port status
        for cs_all_serial_ports in site_serial_port_details['devices']:
            # if x['device_type'] == 'cisco_887':
            #     print("{}\n\n".format(x))
            #     telnet_to_cisco_via_aten(item, int(x['console_port']), username, password)
            # if x['device_type'] == 'cisco_2960':
            #     print("{}\n\n".format(x))
            #     telnet_to_cisco_via_aten(item, int(x['console_port']), username, password)
            # if x['device_type'] == 'nokia_a8':
            #     print("{}\n\n".format(x))
            #     telnet_to_nokia_via_aten(item, int(x['console_port']), username, password)
            if cs_all_serial_ports['device_type'] == 'olt':
                # Ignore serial ports that are decommissioned
                if "dec" not in cs_all_serial_ports['name']:
                    telnet_instance = TelnetOltViaAten(cs_name, int(cs_all_serial_ports['console_port']), username, password)
                    cs_all_serial_ports.update(telnet_instance.summary_result)
                    cons_result.append(cs_all_serial_ports)
                    # print(cons_result)

        write_result_to_file('cons', site_name, {'site':site_name, 'devices':cons_result})

    except Exception as e:
        traceback.print_exc()
        append_log_to_file("aten", cs_name, "*** Caught exception: {}: {}".format(e.__class__, e).split('\n'))
        append_log_to_file("aten", cs_name, traceback.format_exc().split('\n'))
        return


def check_a_serial_port_in_aten(console_variable_list):
    device_name = console_variable_list[0]
    username = console_variable_list[1]
    password = console_variable_list[2]

    site_name = device_name.split('.')[0].split('-')[0]

    cs_name = site_name + '-cons-1.network.cityfibre.com'

    try:
        aten_telnet_instance = TelnetAten(cs_name, username, password)
        if aten_telnet_instance.serial_port_list:
            aten_telnet_instance.check_a_serial_port_in_aten(device_name)
        else:
            print(device_name.split('.')[0].split('-',1))
            x = {'serial_status': 'could-not-check', 'device_subtype': 'unknown',
                                   'serial_substatus': 'unknown', 'label':device_name.split('.')[0].split('-',1)[1]}
            write_result_to_file('olt-selective-cons', site_name + "-port-" + "unknown", x)
    except Exception as e:
        print("*** Caught exception: {}: {}".format(e.__class__, e))
        # pass


def main():
    """
    Collect inventory data from seed file and run `cisco_task` to devices in parallel
    :return: None
    """

    # Number of parallel tasks
    concurrent_thread_number = 20

    # Load seed file to 'inventory'
    parser = argparse.ArgumentParser(description='Read seed file')
    parser.add_argument('--seed', required=True)
    args = parser.parse_args()
    f = open(args.seed, "r")
    inventory = yaml.load(f.read(), Loader=yaml.FullLoader)

    # Load credentials file
    credential_file = open("credentials.yml", "r")
    credentials = yaml.load(credential_file.read(), Loader=yaml.FullLoader)

    # Set username and password for Telnet sessions
    username = credentials["credentials"]["username"]
    password = credentials["credentials"]["password"]
    # password = ask_pass(username)

    #- site:mk938
    #  devices:
    #   - name:test-mgmt-1
    #     console_port:1
    #     device_type:cisco_887
    #   - name:test-mgmtsw-1
    #     console_port:2
    #     device_type:cisco_2960

    # Create variable list for thread task
    console_task_variable_list = []
    for item in inventory["olt"]:
        console_task_variable_list.append([item, username, password])

    # Run multiple tasks as threads in parallel
    pool = ThreadPool(concurrent_thread_number)
    pool.map(check_a_serial_port_in_aten, console_task_variable_list)

    file_list = os.listdir("./olt-selective-cons-results")
    file_list.sort()

    with open('olt-selective-cons-result.csv', 'w', newline='') as myfile:
        wr = csv.writer(myfile, quoting=csv.QUOTE_ALL)
        file_list = os.listdir("./olt-selective-cons-results")
        file_list.sort()
        wr.writerow(['site_name','console_server_port','device_name','device_type','device_serial_status','device_serial_substatus'])
        for filename in file_list:
            site_name = filename.split('-')[0]
            port = filename.split('-')[2]
            with open("./olt-selective-cons-results/" + filename, 'r') as a_file:
                x = yaml.load(a_file.read(), Loader=yaml.FullLoader)
                wr.writerow([site_name, port, x['label'], x['device_subtype'], x['serial_status'], x['serial_substatus']])


if __name__ == '__main__':
    main()

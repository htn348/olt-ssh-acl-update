Introduction
------------
This repository makes use of [paramiko](https://github.com/paramiko/paramiko) and telnetlib to provide a script to update SSH ACL on Calix OLT E9-2 devices in CFH Mgmt Network.

The following tasks are run for each Calix OLT E9-2 device

1. Check console status of Calix OLT E9-2 devices, proceed if they are in place
2. PING test to device to check path quality, proceed if good quality
3. SSH to device
4. Check existing access-list ipv4 CRAFT_INTERFACE_CONTROL content, proceed if matches existing access-list ipv4 CRAFT_INTERFACE_CONTROL
5. Create ip prefix-list SSH-temp with same content as existing ip prefix-list SSH
6. Apply ip prefix-list SSH-temp to access-list ipv4 CRAFT_INTERFACE_CONTROL rule 29
7. Delete access-list ipv4 CRAFT_INTERFACE_CONTROL rule 30
8. Delete ip prefix-list SSH
9. Create ip prefix-list SSH with new content
10. Apply ip prefix-list SSH to access-list ipv4 CRAFT_INTERFACE_CONTROL rule 30
11. Delete access-list ipv4 CRAFT_INTERFACE_CONTROL rule 29
12. Save config to NVRAM

Requirement
-----------
* A Linux machine with git, python 3.6 and [virtualenv](https://pypi.org/project/virtualenv/)
* Access to CFH Mgmt Network, Internet and [CityFibre bitbucket](https://bitbucket.org/cityfibre/)

Usage
-----
Clone this repository:

    git clone https://bitbucket.org/cityfibre/olt-ssh-acl-update

In `olt-ssh-acl-update` folder, run `virtualenv` and install dependencies

    virtualenv venv
    source venv/bin/activate
    pip install -r dev-requirements.txt
    

In `seedFile.yml`

* Change username to your FreeIPA RADIUS username
* Update the list of Calix OLT E9-2 devices to run the script to

Run

    python3 olt_console_selective_check.py --seed seedFile.yml

Type your FreeIPA password at the prompt.
Console check results are in `olt-selective-cons-result.csv`. Other logs are in `stdout` or folders `olt-selective-cons-results` and `cons-logs`

Run

    python3 olt-ssh-acl-update.py --seed seedFile.yml

Type your FreeIPA password at the prompt.
SSH ACL change will then run and produce summarized results in `result.yml` and `result.csv`
Paramiko logs is in `paramiko.log`. Other SSH ACL change script logs are in `stdout` or folders `olt-logs` and `olt-results`.
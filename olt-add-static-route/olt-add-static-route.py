#!/usr/bin/env python3ls
#
# This file makes use of paramiko.
#
# Paramiko is free software; you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation; either version 2.1 of the License, or (at your option)
# any later version.
#
# Paramiko is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with Paramiko; if not, write to the Free Software Foundation, Inc.,
# 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA.

import getpass
import os
import traceback
import argparse
import time
import subprocess
import math
import datetime
from multiprocessing.dummy import Pool as ThreadPool
import collections
import csv
import telnetlib
import pexpect
import json
import yaml
import paramiko
import dns.resolver

"""
Pseudo-code
    1. Check OLT connectivity status
    2. Check current static route - 2 x statics to 10.0.0.0/8 and 192.168.100.0/24 next-hop 10.240.4.1
        Expected next-hop is deduced from the OLT IP address
        abd860-olt-2# sh run ip route
            ip route 10.0.0.0/8 next-hop 10.240.4.1
            ip route 192.168.100.0/24 next-hop 10.240.4.1
    3. Provision new static route
        ip route 0.0.0.0/0 next-hop 10.240.4.1
    4. Check that new static route is in place
        sh run ip route | include 0.0.0.0/0
    5. Remove previous static routes
        no ip route 10.0.0.0/8 next-hop 10.240.4.1
        no ip route 192.168.100.0/24 next-hop 10.240.4.1
    6. Check final content of static routes
        show run ip route
    7. Copy run start
    
"""

def ask_pass(user):
    """
    Get user to type in password
    :param str user: Username
    :return: Password (`str`)
    """
    prompt = "Password for {} to SSH to OLT devices: ".format(user)
    password = getpass.getpass(prompt)
    return password


def send_olt_command(shell, command, sleep_time):
    """
    Send a ``command`` over an established SSH interactive ``shell`` to a OLT device, wait for ``sleep_time`` and
    return received data on the SSH interactive ``shell``
    :param .Channel shell: a paramiko Channel SSH interactive session to a OLT device
    :param str command: a OLT CLI command
    :param float sleep_time: time in seconds to wait for received data over the ``shell`` after sending the command.
    Default is 3 x round trip delay found in `ping_test`.
    :return: received data (`str`) over the SSH interactive ``shell``
    """
    shell.send(command)
    time.sleep(sleep_time)
    output = shell.recv(65535).decode(encoding='utf-8')
    return output


def send_olt_config_file(shell, commandFile, sleep_time):
    """
    Send a OLT CLI commands in ``commandFile`` within 'configure terminal' context of an interactive SSH ``shell``
    :param .Channel shell: a paramiko Channel SSH interactive session to a OLT device
    :param str commandFile: name of a config file in the current working directory
    :param float sleep_time: time in seconds to wait for received data over the ``shell`` after sending each command.
    Default is 3 x round trip delay found in `ping_test`.
    :return: received data (`str`) over the SSH interactive ``shell``
    """
    output = ""
    output += send_olt_command(shell, "configure\n", sleep_time)
    pwd = os.getcwd()
    item_full_path = os.path.join(pwd, commandFile)

    with open(item_full_path, 'r') as f:
        content = f.readlines()
        # Remove whitespace characters like `\n` at the end of each line
        content = [x.strip() for x in content]
        for line in content:
            output += send_olt_command(shell, line + '\n', sleep_time)

    # Send CTRL-Z to end a configuration task on OLT
    output += send_olt_command(shell, chr(26), 1 + sleep_time)
    return output


def read_config_file(configFile):
    """
    Read CLI configurations from ``configFile`` and return a list of CLI commands with whitespace characters removed
    :param str configFile: name of a config file in the current working directory
    :return: CLI commands in a list
    """
    pwd = os.getcwd()
    item_full_path = os.path.join(pwd, configFile)

    with open(item_full_path, 'r') as f:
        content = f.readlines()
        # Remove whitespace characters like `\n` at the end of each line
        content = [x.strip() for x in content]
    return content


def append_log_to_file(device_type, log_file_name, log_content):
    """
    Append ``log_content`` to ``log_file_name`` with timestamp
    :param str device_type: device type to use as log folder name
    :param str log_file_name: name of log file in current working directory
    :param list log_content: log content to write to log file
    :return: ``None``
    """
    pwd = os.getcwd()
    if not os.path.isdir(device_type + '-logs'):
        os.makedirs(device_type + '-logs')
    item_full_path = os.path.join(pwd, device_type + '-logs/' + log_file_name + '.log')

    ts = time.time()
    st = datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d %H:%M:%S')

    INDENT = " "

    with open(item_full_path, 'a') as x:
        x.write('\n[' + st + ']\n')
        for line in log_content:
            x.write(INDENT * 25 + line + '\n')


def write_result_to_file(device_type, log_file_name, log_content):
    """
    Append ``log_content`` to ``log_file_name`` with timestamp
    :param str device_type: device type to use as log folder name
    :param str log_file_name: name of log file in current working directory
    :param list log_content: log content to write to log file
    :return: ``None``
    """
    pwd = os.getcwd()
    if not os.path.isdir(device_type + '-results'):
        os.makedirs(device_type + '-results')
    item_full_path = os.path.join(pwd, device_type + '-results/' + log_file_name + '-result.yml')

    with open(item_full_path, 'w') as outfile:
        yaml.dump(log_content, outfile, default_flow_style=False)
    return


def ping_test(hostname, number_of_packets):
    """
    Perform ICMP test to ``hostname`` with ``number_of_packets`` times and return max latency if there are no loss,
    and ``None`` otherwise.
    :param str hostname: Target of ICMP echo requests, a DNS name or IP address
    :param int number_of_packets: Number of ICMP echo request packets
    :return: If there's no packet loss, return a list of two items, max latency in round up milliseconds
    and full decoded ping response. If there's packet loss, return ``None``
    """
    try:
        original_ping_response = subprocess.check_output(["ping", "-c", str(number_of_packets), "-W", "2", hostname])
    except subprocess.CalledProcessError as e:
        # raise RuntimeError("command '{}' return with error (code {}): {}".format(e.cmd, e.returncode, e.output))
        return ['ping_fail',['ping_fail']]
    decoded_ping_response = original_ping_response.decode("utf-8").split("\n")

    number_of_lines_with_time = 0
    for line in decoded_ping_response:
        if "time" in line:
            number_of_lines_with_time += 1

    if number_of_lines_with_time == number_of_packets + 1:
        rtt_text = None
        for line in decoded_ping_response:
            if "rtt" in line:
                rtt_text = line
                break

        max_latency_string = rtt_text.split("=")[1].split("/")[2]
        max_latency_integer = math.ceil(int(float(max_latency_string)))
        return [max_latency_integer, decoded_ping_response]
    else:
        return ['ping_fail', decoded_ping_response]


def olt_task(olt_variable_list):
    """
    1. Check OLT connectivity status
    2. Check current static route - 2 x statics to 10.0.0.0/8 and 192.168.100.0/24
        sh run ip route
            ip route 10.0.0.0/8 next-hop 10.240.4.1
            ip route 192.168.100.0/24 next-hop 10.240.4.1
    3. Provision new static route
        ip route 0.0.0.0/0 next-hop 10.240.4.1
    4. Check that new static route is in place
        sh run ip route | include 0.0.0.0/0
    5. Remove previous static routes
        no ip route 10.0.0.0/8 next-hop 10.240.4.1
        no ip route 192.168.100.0/24 next-hop 10.240.4.1
    6. Check final content of static routes
        show run ip route
    7. Copy run start
    :param list olt_variable_list: a list of hostname, username, password and device type of a device
    :return: ``None``
    """
    # Setup paramiko logging to a file in current working directory
    paramiko.util.log_to_file("paramiko.log")

    # Paramiko client configuration
    UseGSSAPI = (
        paramiko.GSS_AUTH_AVAILABLE
    )  # enable "gssapi-with-mic" authentication, if supported by your python installation
    DoGSSAPIKeyExchange = (
        paramiko.GSS_AUTH_AVAILABLE
    )  # enable "gssapi-kex" key exchange, if supported by your python installation

    port = 22

    hostname = olt_variable_list[0]
    username = olt_variable_list[1]
    password = olt_variable_list[2]
    device_type = olt_variable_list[3]

    result = {}

    number_of_pings = 1  # Number of pings in a PING test
    delay_multiplier = 3  # Multiplier to deduce normal_cli_delay from number_of_pings

    normal_cli_delay = 0  # Delay for each CLI command, will be set after PING test is finished
    show_run_delay = 5  # Additional delay for a 'show run' CLI command
    show_run_delay_short = 1
    copy_run_start_delay = 5  # Additional delay for a 'copy run start' command

    expected_static_route_prefixes = ['10.0.0.0/8','192.168.100.0/24']
    NEXTHOP = ""

    # 1. Test link quality by ping test
    print(hostname + " - " + "PING test started.")
    append_log_to_file(device_type, hostname, "PING test started.".split('\n'))
    try:
        ping_result = ping_test(hostname, number_of_pings)
        if ping_result[0] == 'ping_fail':
            print(hostname + " - " + "PING test finished. Poor link quality. Change aborted.")
            append_log_to_file(device_type,
                               hostname,
                               "PING test finished. Poor link quality. Change aborted.".split('\n')
                               )
            append_log_to_file(device_type, hostname, ping_result[1])
            result.update({'host':hostname, 'status':'SCRIPT_NOT_RUN','reason':'Ping Test failed'})
            write_result_to_file(device_type,hostname,result)
            return
        else:
            print(hostname + " - " + "PING test finished. PING test successful.")
            append_log_to_file(device_type, hostname, "PING test finished. PING test successful.".split('\n'))
            append_log_to_file(device_type, hostname, ping_result[1])
            normal_cli_delay = ping_result[0] * delay_multiplier / 1000
            # normal_cli_delay = math.ceil(ping_result[0] * delay_multiplier / 1000)
            print(normal_cli_delay)
            print(hostname + " - " + "PING result - max latency " + str(ping_result[0]) + " ms")
            append_log_to_file(device_type,
                               hostname,
                               ("PING result - max latency " + str(ping_result[0]) + " ms").split('\n')
                               )

    except Exception as e:
        # print("*** Caught exception: %s: %s" % (e.__class__, e))
        traceback.print_exc()
        result.update({'host': hostname, 'status': 'SCRIPT_NOT_RUN', 'reason': 'Caught Exception'})
        with open(hostname + '-result.yml', 'w') as outfile:
            yaml.dump(result, outfile, default_flow_style=False)
        return

    try:
        # SSH to device
        client = paramiko.SSHClient()
        client.load_system_host_keys()
        client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        print(hostname + " - " + "SSH - Connecting using RADIUS account...")
        append_log_to_file(device_type, hostname, 'SSH - Connecting using RADIUS account...'.split('\n'))
        if not UseGSSAPI and not DoGSSAPIKeyExchange:
            client.connect(hostname, port, username, password, look_for_keys=False, allow_agent=False)
        else:
            try:
                client.connect(
                    hostname,
                    port,
                    username,
                    gss_auth=UseGSSAPI,
                    gss_kex=DoGSSAPIKeyExchange,
                )
            except Exception:
                traceback.print_exc()
                password = getpass.getpass(
                    "Password for %s@%s: " % (username, hostname)
                )
                client.connect(hostname, port, username, password)

        print(hostname + " - " + "SSH connected.")
        append_log_to_file(device_type, hostname, 'SSH Connected'.split('\n'))

        # Invoke an SSH shell over the established SSH Transport
        shell = client.invoke_shell()
        time.sleep(3)
        output = shell.recv(65535).decode(encoding='utf-8').split('\n')

        print(output)

        for line in output:
            if ">" in line:
                append_log_to_file(device_type, hostname,
                                   "Calix E7. Change aborted".split('\n'))
                result.update({'host': hostname, 'status': 'SCRIPT_RUN_AND_FAILED', 'reason': 'E7'})
                write_result_to_file(device_type, hostname, result)
                client.close()
                return

        print("{} - {}".format(hostname,output))
        append_log_to_file(device_type, hostname, output)

        term_len_0 = send_olt_command(shell,
                                        "terminal screen-length 0\n",
                                      normal_cli_delay
                                      ).split('\r\n')
        print(term_len_0)
        append_log_to_file(device_type, hostname, term_len_0)

        # 2. Verify current static routes are as expected

        answers = dns.resolver.query(hostname, 'A')
        olt_expected_default_gateway = ''
        for rdata in answers:
            rdata_str = str(rdata)
            olt_expected_default_gateway = rdata_str[:rdata_str.rfind('.')] + '.1'
            break

        existing_show_run_ip_route = send_olt_command(shell, "sh run ip route | display json\n", show_run_delay_short + normal_cli_delay).split('\r\n')
        time.sleep(1)

        append_log_to_file(device_type, hostname, existing_show_run_ip_route)

        existing_show_run_ip_route = [x.strip() for x in existing_show_run_ip_route[1:-1]]
        for item in json.loads(''.join(existing_show_run_ip_route))['data']['exa-base:config']['system']['ip']['route']:
            if item['destination-prefix'] and item['destination-prefix'] in expected_static_route_prefixes:
                print("{} - {}".format(hostname, "Expected prefix"))
                if item['next-hop'] == olt_expected_default_gateway:
                    NEXTHOP = item['next-hop']
                    print("{} - {}".format(hostname, "Expected nexthop"))
                else:
                    print("{} - {}".format(hostname, "Unexpected nexthop"))
                    result.update({'host': hostname, 'status': 'SCRIPT_RUN_AND_FAILED',
                                   'reason': 'Unexpected nexthop'})
                    write_result_to_file(device_type, hostname, result)
                    client.close()
                    return
            else:
                print("{} - {}".format(hostname, "Unexpected prefix"))
                result.update({'host': hostname, 'status': 'SCRIPT_RUN_AND_FAILED',
                               'reason': 'Unexpected nexthop'})
                write_result_to_file(device_type, hostname, result)
                client.close()
                return

        # 3. Configure new static route
        output = send_olt_command(shell, "configure\n", normal_cli_delay).split('\r\n')
        # Send CTRL-Z to end a configuration task on OLT
        output += send_olt_command(shell,"ip route 0.0.0.0/0 next-hop " + NEXTHOP + "\n",
                                                  show_run_delay_short + normal_cli_delay).split('\r\n')
        output += send_olt_command(shell, chr(26), 1 + normal_cli_delay).split('\r\n')

        print("{} - {}".format(hostname, output))
        append_log_to_file(device_type, hostname, output)

        time.sleep(1)

        # 4. Confirm that new static route is in place
        existing_show_run_ip_route = send_olt_command(shell, "sh run ip route | display json\n", show_run_delay_short + normal_cli_delay).split('\r\n')
        time.sleep(1)

        append_log_to_file(device_type, hostname, existing_show_run_ip_route)

        existing_show_run_ip_route = [x.strip() for x in existing_show_run_ip_route[1:-1]]
        i = 0
        for item in json.loads(''.join(existing_show_run_ip_route))['data']['exa-base:config']['system']['ip']['route']:
            if item['destination-prefix'] and item['destination-prefix'] == '0.0.0.0/0':
                if item['next-hop'] == NEXTHOP:
                    i = 1
                    break
        if i == 1:
            print("{} - {}".format(hostname, "New static route is in place"))
            append_log_to_file(device_type, hostname, "New static route is in place".split("\n"))
        else:
            print("{} - {}".format(hostname, "New static route is not in place"))
            append_log_to_file(device_type, hostname, "New static route is not in place".split("\n"))
            result.update(
                {'host': hostname, 'status': 'SCRIPT_RUN_AND_FAILED', 'reason': 'New static route is not in place'})
            write_result_to_file(device_type, hostname, result)
            client.close()
            return

        # 5. Remove unused static routes
        output += send_olt_command(shell, "configure\n", normal_cli_delay).split('\r\n')
        # Send CTRL-Z to end a configuration task on OLT
        output += send_olt_command(shell,"no ip route 10.0.0.0/8 next-hop " + NEXTHOP + "\n",
                                                  show_run_delay_short + normal_cli_delay).split('\r\n')
        output += send_olt_command(shell, "no ip route 192.168.100.0/24 next-hop " + NEXTHOP + "\n",
                                   show_run_delay_short + normal_cli_delay).split('\r\n')
        output += send_olt_command(shell, chr(26), 1 + normal_cli_delay).split('\r\n')

        # output = send_olt_config_file(shell, 'temp-ip-prefix-list', normal_cli_delay).split('\r\n')
        print("{} - {}".format(hostname, output))
        append_log_to_file(device_type, hostname, output)

        time.sleep(1)

        # 6. Confirm final static route content
        existing_show_run_ip_route = send_olt_command(shell, "sh run ip route | display json\n", show_run_delay_short + normal_cli_delay).split('\r\n')
        time.sleep(1)

        append_log_to_file(device_type, hostname, existing_show_run_ip_route)

        existing_show_run_ip_route = [x.strip() for x in existing_show_run_ip_route[1:-1]]
        static_route_list = json.loads(''.join(existing_show_run_ip_route))['data']['exa-base:config']['system']['ip']['route']
        if len(static_route_list) == 1:
            print("One static route remains.")
        i = 0
        for item in static_route_list:
            if item['destination-prefix'] and item['destination-prefix'] == '0.0.0.0/0':
                if item['next-hop'] == NEXTHOP:
                    i = 1
                    break
        if i == 1:
            print("New static route is in place")

        # 7. Copy run start
        output = send_olt_command(shell, "copy running-config startup-config \n", copy_run_start_delay + normal_cli_delay).split('\r\n')
        print("{} - {}".format(hostname, output))
        append_log_to_file(device_type, hostname, output)

        result.update({'host': hostname, 'status': 'SCRIPT_RUN_AND_PASSED', 'reason': 'None'})
        write_result_to_file(device_type, hostname, result)
        client.close()

    except paramiko.AuthenticationException as e:
        print("{} - SSH authentication failed.".format(hostname))
        append_log_to_file(device_type, hostname, "SSH authentication failed".split('\n'))
        append_log_to_file(device_type, hostname, "*** Caught exception: {}: {}".format(e.__class__, e).split('\n'))
        append_log_to_file(device_type, hostname, traceback.format_exc().split('\n'))

        result.update({'host': hostname, 'status': 'SCRIPT_NOT_RUN', 'reason': 'Caught Exception'})
        write_result_to_file(device_type, hostname, result)
        try:
            client.close()
        except:
            pass
        return

    except Exception as e:
        print("*** Caught exception: %s: %s" % (e.__class__, e))
        traceback.print_exc()
        append_log_to_file(device_type, hostname, "*** Caught exception: {}: {}".format(e.__class__, e).split('\n'))
        append_log_to_file(device_type, hostname, traceback.format_exc().split('\n'))

        result.update({'host': hostname, 'status': 'SCRIPT_NOT_RUN', 'reason': 'Caught Exception'})
        write_result_to_file(device_type, hostname, result)

        try:
            client.close()
        except:
            pass
        return


def main():
    """
    Collect inventory data from seed file and run `cisco_task` to devices in parallel
    :return: None
    """

    # Number of parallel tasks
    concurrent_thread_number = 30

    # Load seed file to 'inventory'
    parser = argparse.ArgumentParser(description='Read seed file')
    parser.add_argument('--seed', required=True)
    args = parser.parse_args()
    f = open(args.seed, "r")
    inventory = yaml.load(f.read(), Loader=yaml.FullLoader)

    # Load credentials file
    credential_file = open("../credentials.yml", "r")
    credentials = yaml.load(credential_file.read(), Loader=yaml.FullLoader)

    # Set username and password for Telnet sessions
    username = credentials["credentials"]["username"]
    password = credentials["credentials"]["password"]
    # password = ask_pass(username)

    # Create variable list for thread task
    olt_task_variable_list = []
    for item in inventory["olt"]:
        olt_task_variable_list.append([item, username, password, "olt"])

    # Run multiple tasks as threads in parallel
    pool = ThreadPool(concurrent_thread_number)
    pool.map(olt_task, olt_task_variable_list)

    data_to_write = {}
    temp_list = []
    for item in inventory["olt"]:
        try:
            f = open('olt-results/' + item + "-result.yml", "r")
            x = yaml.load(f.read(), Loader=yaml.FullLoader)
            temp_list.append(x)
        except:
            pass
    data_to_write['olt'] = temp_list

    with open('result.yml', 'w') as outfile:
        yaml.safe_dump(data_to_write, outfile, default_flow_style=False)

    with open('result.csv', 'w', newline='') as myfile:
        wr = csv.writer(myfile, quoting=csv.QUOTE_ALL)
        for line in data_to_write["olt"]:
            wr.writerow([line["host"], line["status"], line["reason"]])


if __name__ == '__main__':
    main()
